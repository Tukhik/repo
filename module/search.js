const USER_PER_PAGE = 20;
export class Search {
	setCurrentPage(pageNumber){
		this.currentPage = pageNumber;
	}
	constructor(view){
		this.view = view;
		this.view.searchInput.addEventListener('keyup', this.loadUsers.bind(this));
		this.view.loadMore.addEventListener('click', this.loadUsers.bind(this));
		this.currentPage = 1;
	}

	async loadUsers(e){
		if(this.currentPage == 1){
			if(e.key == "Enter"){
				const searchValue =this.view.searchInput.value;
		if(searchValue){

		return await fetch(`https://api.github.com/search/users?q=${searchValue}&per_page=${USER_PER_PAGE}&page=${this.currentPage}`)
		.then((res)=> {
			if(res.ok){
				this.setCurrentPage(this.currentPage +1)
				res.json().then(res =>{
					res.items.forEach(user => this.view.createUser(user));
				})

			}
			else {
				console.log('error')
			}
			})
		} else {
			this.clearUsers();
		}
	}
} else {
	const searchValue =this.view.searchInput.value;
	if(searchValue){

	return await fetch(`https://api.github.com/search/users?q=${searchValue}&per_page=${USER_PER_PAGE}&page=${this.currentPage}`)
	.then((res)=> {
		if(res.ok){
			this.setCurrentPage(this.currentPage +1)
			res.json().then(res =>{
				this.view.toogleLoadeMore 
				res.items.forEach(user => this.view.createUser(user));
			})
		} else {
			console.log('error')
			}
		})
	} else {
		this.clearUsers();
		this.currentPage = 1;
	}
	}
}
clearUsers(){
	this.view.usersList.innerHTML = ''; 
	this.view.loadMore.style.display = 'none';
	}

}
